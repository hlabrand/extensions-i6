!---------------------------------------------------------------------------
!              FrenchGAugmented
!
!        par   Hugo Labrande
!
! (Licence : Domaine public)
!
!---------------------------------------------------------------------------
!
!    Cette extension d�finit des verbes suppl�mentaires par rapport �
!  FrenchG.h, qui ne s'occupe que des verbes d�finis dans les biblioth�ques
!  I6. Tout n'est pas forc�ment utile � tout le monde tout le temps, alors
!  je vous encourage de modifier cette biblioth�que pour votre usage
!  personnel. Et comme j'ai piqu� des bouts de code � droite � gauche, ce
!  fichier est dans le domaine public.
!
!    Ce fichier comprend :
!      - des nouveaux verbes : utiliser, actionner, essayer, s'approcher
!        (de quelque chose), afficher (infos du jeu), activer et
!        d�sactiver, mordre, incin�rer & cuire, se servir de,
!       Note: le verbe "utiliser" est implant� en tant que clic-droit �
!             la LucasArts
!      - des formulations en plus : prier Allah, vider poches, tout mettre,
!        tout prendre, tout acheter, tout enlever, quitter un pull
!      - des distinctions de verbes :
!         * 'secouer' n'est pas toujours �gal � 'agiter'
!         * 'aller vers' n'est pas la m�me chose que 'entrer'
!
!---------------------------------------------------------------------------
!
!    Utilisation : rajoutez
!          Include "FrenchGAugmented";
!    � la ligne juste apr�s Include "FrenchG" dans votre code.
!
!---------------------------------------------------------------------------



!---------------------------------------------------------------------------
!  NOUVEAUX VERBES



!-------------------
! Actionner
!    Pr�sent dans la biblioth�que francophone comme synonyme d'utiliser qui renvoie aussi "soyez plus pr�cis"

[ ActionnerSub;
  if (noun has switchable && noun has on) {print "(�teindre ", (the) noun, ")^"; <<SwitchOff noun>>;}
  if (noun has switchable && noun hasnt on) {print "(allumer ", (the) noun, ")^"; <<SwitchOn noun>>;}
  "Il ne s'agit pas de quelque chose que vous pouvez actionner.";
];

Extend only 'actionner' replace
* noun				-> Actionner;

!-------------------
! Activer et d�sactiver		(inspir� par ExpertGrammar.h d'Emily Short)

Verb 'activer'
* noun				-> SwitchOn;

Verb 'desactiver'
* noun				-> SwitchOff;



!-------------------
! Afficher
!    Pour afficher les informations relatives au jeu	(inspir� de ExpertGrammar.h d'Emily Short)

Verb meta 'afficher' 'affichage'
* 'score'							-> Score
* 'score' 'detaille'/'complet'		-> FullScore
* 'version'							-> Version
* 'version' 'fr'/'vf'/'francaise'	-> VersionFR
* 'vf'/'fr'							-> VersionFR
* 'pronoms'/'noms'					-> Pronouns
* 'tours'							-> Score
* 'verification'/'integrite'		-> Verify
* 'court'/'bref'					-> LMode1
* 'normal'							-> LMode2
* 'long'/'verbeux'/'bavard'			-> LMode3;



!-------------------
! S'approcher
!    Note aux d�veloppeurs : se rapprocher de quelqu'un peut �tre compris
!                            comme une tentative de Kiss

[ ApprocherSub ;
  if (noun == nothing) {"Pr�cisez de quoi vous voulez vous approcher.";}
  else {"Vous n'avez pas vraiment besoin d'�tre plus pr�s.";}
];

Verb 'approcher' 'rapprocher'
*				-> Approcher
* 'vous'		-> Approcher
* 'vous' noun	-> Approcher
* 'vous' 'de'/'d^'/'du'/'des' noun -> Approcher;




!-------------------
! Avancer
!   Compris comme synonyme d'aller au nord, mais comme c'est pas forc�ment le cas il faut le dire pour pas
!     que le joueur soit confus sans savoir pourquoi

[ AvancerSub;
  print "(aller au nord)^"; <<Go n_obj>>;
];

Verb 'avancer'
*					-> Avancer;


!-------------------
! Cuire et Incin�rer	(inspir� par ExpertGrammar.h d'Emily Short)

Verb 'cuire' 'incinerer' = 'bruler';



!-------------------
! Essayer			(inspir� par ExpertGrammar.h de Emily Short)
!    'essayer le pull' ou 'essayer la porte de gauche'

[ EssayerSub;
  if (noun has door) {<<Open noun>>;}
  if (noun has clothing) {<<Wear noun>>;}
  "Je ne comprends pas cette phrase.";
];

Verb 'essayer'
* noun				-> Essayer;



!-------------------
! Mordre			(inspir� par ExpertGrammar.h d'Emily Short)

Verb 'mordre'
* edible			-> Eat
* 'dans' edible		-> Eat
* noun				-> Attack;



!-------------------
! Prier un dieu		(inspir� par ExpertGrammar.h d'Emily Short)

Extend 'prier'
* topic				-> Pray
* 'a' topic			-> Pray;


!-------------------
! Quitter un v�tement (c'est dur car quitter est meta, mais voir http://www.firthworks.com/roger/informfaq/vv.html)

[ isMeta; meta = true; return GPR_PREPOSITION; ];

Extend 'quitter' first
* isMeta		-> Quit
* held		    -> Disrobe;



!--------------------
! Tout mettre, tout prendre, etc
! La solution : d�finir un token tout, qui matche n'importe quoi qui suit "tout",
!               puis dans ce token on r��crit tout juste apr�s le verbe,
!               et on reparse, pour que le parser d�clenche "prendre tout"

[ ToutSub;
  "Si vous voyez cela, il y a un bug.";
];

[ tout b;
  b = LongueurMot(1)+PositionMot(1);
  EcraseMot(0);
  LTI_Insert(b, ' ');
  LTI_Insert(b+1, 't');
  LTI_Insert(b+2, 'o');
  LTI_Insert(b+3, 'u');
  LTI_Insert(b+4, 't');
  Tokenise__(buffer,parse);
  return GPR_REPARSE;
];

Verb 'tout'
* tout					-> Tout;




!--------------------
! Vider poches
!    Synonyme de tout poser par terre ou tout mettre quelque part

[ ViderPochesSub ;
  "Si vous voyez cela, il y a un bug.";
];

[ poche b n ok i j k;
  ! V�rifier qu'on a la bonne forme
  n = NbMots(); ok=0;
  if (n > 1) {
    if (Mot(1) == 'poche' or 'poches' or 'inventaire') {ok=1;}
	if (n > 2) {
		if (Mot(1) == 'mes' or 'ses' or 'ma' or 'sa' && Mot(2) == 'poche' or 'poches' or 'inventaire') {ok=2;}
	}
  }
  if (ok == 0) {return GPR_FAIL;}
  else {
	  ! transformer "vider poches" en "poser tout"
	  b = PositionMot(0);
	  buffer->b = 'p'; buffer->(b+1) = 'o'; buffer->(b+2)='s';

	  b = PositionMot(1); EcraseMot(1); if (ok==2) {EcraseMot(2);}
	  LTI_Insert(b, ' ');
	  LTI_Insert(b+1, 't');
	  LTI_Insert(b+2, 'o');
	  LTI_Insert(b+3, 'u');
	  LTI_Insert(b+4, 't');
  	  Tokenise__(buffer,parse);

	  ! Afficher la traduction correcte pour aider le joueur
	  print "(";
	  for (i=0 : i<NbMots() : i++) {
		if (i>0) {print " ";}
		j = PositionMot(i); for (k = 0 : k < LongueurMot(i) : k++) { print (char) buffer->(j+k);}
	  }
	  print ")^";

  	  return GPR_REPARSE;
  }
];

Extend 'vider'
* poche			->ViderPoches;


!---------------------------------------------------------------------------
!  Utiliser et Se servir de
!    (trouv� dans "Searching for Iain" d'Alex Watson,
!            http://caskly.org/alex/archive/ifgames/iain.inf)
!    Dans la biblioth�que francophone, on renvoie toujours "soyez plus pr�cis"
!    Je pr�f�re cette version, � la "clic-droit de LucasArts"

[ UtiliserSub;
  if (noun has edible) {print "(manger ", (the) noun, ")^"; <<Eat noun>>;}
  if (noun has clothing) {print "(enfiler ", (the) noun, ")^"; <<Wear noun>>;}
  if (noun has switchable && noun has on) {print "(�teindre ", (the) noun, ")^"; <<SwitchOff noun>>;}
  if (noun has switchable && noun hasnt on) {print "(allumer ", (the) noun, ")^"; <<SwitchOn noun>>;}
  if (noun has talkable || noun has animate) {print "(parler ", (to_the) noun, ")^"; <<ParlerSansPrecision noun>>;}
  if (noun has door) {print "(ouvrir ", (the) noun, ")^"; <<Open noun>>;}
  if (noun has openable && noun has open) {print "(fermer ", (the) noun, ")^"; <<Close noun>>;}
  if (noun has openable && noun hasnt open) {print "(ouvrir ", (the) noun, ")^"; <<Open noun>>;}
  if (noun has container) {print "(vider ", (the) noun, ")^"; <<Empty noun>>;}
  "Il va falloir �tre plus pr�cis sur la fa�on dont vous voulez utiliser ", (the) noun, ".";
];

[ UtiliserDeuxSub;
  if (noun has container && second has container)
   {print "(vider ", (the) noun, " dans ", (the) second, ")^"; <<EmptyT noun second>>;}
  if (noun in player && second has container)
   {print "(mettre ", (the) noun, " dans ", (the) second, ")^"; <<Insert noun second>>;}
  if (noun in player && (second has animate || second has talkable))
   {print "(donner ", (the) noun, " ", (to_the) second, ")^"; <<Give noun second>>;}
  "Il va falloir �tre plus pr�cis sur la fa�on dont vous voulez utiliser ", (the) noun, " avec ", (the) second, ".";
];

Extend only 'utiliser' replace
* noun				-> Utiliser
* noun 'avec' noun	-> UtiliserDeux;

Verb 'servir'
* 'vous' 'de'/'du'/'des'/'d^' noun				-> Utiliser
* 'vous' 'de'/'du'/'des'/'d^' noun 'avec' noun	-> UtiliserDeux;



!---------------------------------------------------------------------------
!  DISTINCTIONS



!-------------------
! Secouer : 'secouer' n'a pas toujours le m�me sens que 'agiter' : par exemple
!            secouer distributeur de cannettes
!            secouer George (pour le r�veiller ou le secouer physiquement)

[ SecouerSub k;
  ! On le construit comme �tant un synonyme de Wave dans la plupart des cas sauf si l'objet est lourd (ou cach�) ;
  !   si il est lourd, il est probable que le joueur veut l'attraper et le secouer, pas le prendre/soulever et l'agiter.
  if (noun notin player && noun hasnt scenery && noun hasnt concealed) {
    print "(vous prenez d'abord "; print (the) noun; print ")^";
    k = keep_silent; keep_silent=true; <Take noun>; keep_silent=k;
  }
  if (noun in player) {
	<<Wave noun>>;
  }
  else { "Vous pr�f�rez ne pas y toucher."; }
];

Extend only 'secouer' replace
* creature						 -> Attack
* noun							 -> Secouer;



!-------------------
! Aller vers : 'aller vers' est affect� � Enter, tout comme 'entrer', ce qui donne "entrer dans le personnage" comme synonyme d'aller vers lui

Extend 'aller' first
* creature			-> Approcher
* 'vers' creature	-> Approcher;