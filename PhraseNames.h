!---------------------------------------------------------------------------
!              PhraseNames
!
!        par/by   Hugo Labrande     (pr�nom/firstname at hlabrande dot fr)
!
!   Version : 29/11/2014
!
!  (Licence : Domaine public / public domain)
!
!---------------------------------------------------------------------------

! Doc FR:
!
!   Cette extension permet d'ajouter des 'phrase_names' � votre objet, c'est
!  � dire des bouts de phrase / groupes de mots qui font r�f�rence � votre
!  objet. Avec cette extension, vous pouvez faire en sorte que seulement
!  "barre" et "barre de fer", mais pas "barre de" ou "fer", soient reconnus.
!
!   Elle est inspir�e de 'pname.h' par Neil Cerutti, une (vieille) extension
!  qui permettait de faire, en gros, la m�me chose. Cependant, cette
!  extension avait une syntaxe diff�rente, et la fa�on dont elle fut cod�e
!  fit qu'elle n'est compatible qu'avec la version 6.10 de la biblioth�que
!  Inform.
!
!
!   Pour ceux qui n'aiment pas lire les documentations, voil� un exemple :
!
!    Object barredefer "barre de fer"
!      with parse_name ComplicatedParsing,     ! obligatoire
!           name 'barre' 'barreau',
!           phrase_name '.name' '.opt(' 'de' '.or' 'en' 'fer' '.)';
!
!   reconna�t "barre de fer", "barreau de fer", "barre", "barreau", "barre en
!   fer" et "barreau en fer". Le nombre maximal de mots dans phrase_name est
!   32, ce qui n'est parfois pas assez ; vous pouvez donc �galement d�finir une
!   propri�t� phrase_name2 pour d�finir d'autres phrases suppl�mentaires.
!
!
!
!   La pr�sente extension est bi-plateforme et fonctionne avec n'importe quelle
!  biblioth�que, et ajoute seulement 1Ko � votre jeu. Vous pouvez l'inclure
!  n'importe o� dans votre code apr�s
!    Include "Parser";
!   Une fois l'extension inclue, vous pourrez d�finir une propri�t� 'phrase_name'
!  pour n'importe quel de vos objets ; cette propri�t� contiendra vos bouts de
!  phrase correspondants � l'objet. Pour activer la reconnaissance de ces bouts
!  de phrase, il faut **IMPERATIVEMENT** que vous remplaciez la routine parse_name
!  de l'objet par ComplicatedParsing.
!
!   Qu'est-ce que vous devez mettre dans phrase_name ? Il faut mettre chaque mot
!  de votre bout de phrase entre apostrophes ('barre' 'de' 'fer'), sans virgule
!  entre les mots, et s�parer chacune de vos phrases par des './'. Le point qui
!  est devant signifie que c'est un mot sp�cial dont l'extension se sert.
!   Par exemple, vous pouvez �crire :
!    Object barredefer "barre de fer"
!      with parse_name ComplicatedParsing,
!           phrase_name 'barre' 'de' 'fer' './'
!                       'barre' 'en' 'fer',
!    has female;
!
!   Note : attention, phrase_name ne remplace pas name, et les deux pevent se
!    combiner. ComplicatedParsing commence par lancer une routine parse_name
!    "normale", puis regarde vos phrase_names, et renvoie le score maximal ; ceci
!    pour que vous puissiez par exemple remplacer parse_name par ComplicatedParsing
!    pour tous vos objets sans probl�mes.
!    Ce qui veut dire qu'il faut faire attention quand vous combinez les deux :
!    en r�gle g�n�rale, ne mettez pas d'adjectifs dans votre 'name', mais juste des
!    synonymes pour votre mot.
!
!   Quels sont les diff�rents "mots sp�ciaux" / tokens que vous pouvez utiliser ?
!
!    - './' : s�pare deux bouts de phrase
!
!    - '.name' : le mot est OK si il appara�t dans la propri�t� "name" (utile si
!      vous avez des synonymes pour le mot et que vous ne voulez pas tout r��crire).
!
!    - '.opt' : il y a deux syntaxes :
!      - '.opt' 'mot' : le mot qui suit est optionnel
!      - '.opt(' 'mot1' 'mot2' '.)' : la phrase entre les parenth�ses est optionnelle.
!         ATTENTION: ne pas mettre un .opt( dans un autre .opt( !
!                       (mais vous pouvez mettre un .opt ou un .or dans un .opt()
!
!    - '.rest' : le reste de la phrase est soit pr�sent en entier, soit pas pr�sent
!      du tout (�quivalent � un '.opt(' ... '.)' './')
!
!     - '.or'  : le mot qui suit est une alternative au mot d'avant ; on peut les
!       mettre les uns � la suite des autres
!
!
!

! Doc EN:
!
!   This extension allows you to add "phrase_names" to your object, i.e. bits of
!  sentences / groups of words that refer to your object. With this extension, you
!  can make it so that "fountain" and "fountain of youth" are recognized, but not
!  "fountain of" or "youth".
!
!   It is inspired by 'pname.h' by Neil Cerutti, an (old) extension that allowed
!  you to, in essence, do the same thing. However this extension had a different
!  syntax, and the way it was coded made it incompatible with anything other than
!  version 6.10 of the Inform library.
!
!
!   For those who don't like documentation, here's a quick example:
!
!    Object woodenchair "wooden chair"
!      with parse_name ComplicatedParsing,     ! mandatory
!           name 'chair' 'seat',
!           phrase_name '.name' '.opt(' 'made' 'of' 'wood' '.or' 'oak' '.)' './'
!                       'wooden' '.or' 'oak' '.name';
!
!   This recognizes "wooden chair", "wooden seat", "oak chair", "seat made of
!    wood", "chair made of oak", "chair", "seat", etc. The maximal number of
!    words or tokens you can put in phrase_name is 32, like any property array;
!    should you need more, you can define the property phrase_name2 and add more
!    phrases there.
!
!
!   This extension is bi-platform and works with any library, and adds 1Kb to the
!  size of your game; just include it in your code any time after
!    Include "Parser";
!   Once the extension is included, you can define a 'phrase_name' property for
!  any of your objects ; this property will hold all groups of words corresponding
!  to the object. To activate the recognition of such groups of words, you
!  **ABSOLUTELY MUST** replace the parse_name routine of the object by
!  ComplicatedParsing.
!
!   What can you put in 'phrase_name'? Put each word of your sentence between
!  single quotes ('fountain' 'of' 'youth'), without commas between the words, and
!  separate your sentences by './'. (The leading period means it's a special token
!  defined by the extension). For instance :
!    Object fountainofyouth "fountain of youth"
!      with parse_name ComplicatedParsing,
!           phrase_name 'fountain' './' 'fountain' 'of' 'youth',
!    has ;
!
!   Note: 'phrase_name' doesn't replace 'name', and you can combine both.
!  ComplicatedParsing starts by running a 'regular' parse_name routine, then look
!  at your phrase_names, then sends back the max score ; this is so that you can
!  replace parse_name by ComplicatedParsing for all your objects with no problems.
!   Which also means you have to be careful when combining both; as a general rule,
!  don't put adjectives in your 'name', just synonyms for your noun.
!
!  What are the different tokens / "special words" you can use ?
!
!    - './': separates two groups of words
!
!    - '.name': matches words that appear in the "name" property (useful if the
!      name of the object has synonyms and you don't want to rewrite everything).
!
!     - '.opt': two syntaxes
!        - '.opt' 'word': the following word is optional
!        - '.opt(' 'word1' 'word1' '.)': the group of words in brackets is optional
!             WARNING: do not nest several .opt( (but you can use .opt and .or)
!
!    - '.rest': the rest of the sentence is optional
!               (equivalent to '.opt(' ... '.)' './')
!
!     - '.or' : the following word is an alternative for the previous word (you can
!       combine several in a row)
!
!



! TODO :
!   '.or(' ? mais alors il faut des parenth�ses fermantes pour opt ou pour or ? ou alors juste regarder le contenu des flags?




Property phrase_name 0;
Property phrase_name2 0;        ! Only 32 words available in the phrase_name property
! If you need to declare more, go inside ComplicatedParsing and add your own "if (self provides phrase_name*)" bloc




[ Word n; ! valeur (dictionnaire) du mot num�ro n de parse, avec le premier = num�ro 1
#Ifdef TARGET_ZCODE;
    return parse-->(n*2 - 1);
#Ifnot; ! TARGET_GLULX
    return parse-->(n*3 - 2);
#Endif; ! TARGET_
];

[ tokenCount;
#Ifdef TARGET_ZCODE;
    return parse->1;
#Ifnot; ! TARGET_GLULX
    return parse-->0;
#Endif; ! TARGET_
];

[ ReplaceSingleLetter l;
  switch(l)
  {
    'a': return 'a//';
	'b': return 'b//';
	'c': return 'c//';
	'd': return 'd//';
	'e': return 'e//';
	'f': return 'f//';
	'g': return 'g//';
	'h': return 'h//';
	'i': return 'i//';
	'j': return 'j//';
	'k': return 'k//';
	'l': return 'l//';
	'm': return 'm//';
	'n': return 'n//';
	'o': return 'o//';
	'p': return 'p//';
	'q': return 'q//';
	'r': return 'r//';
	's': return 's//';
	't': return 't//';
	'u': return 'u//';
	'v': return 'v//';
	'w': return 'w//';
	'x': return 'x//';
	'y': return 'y//';
	'z': return 'z//';
  }
];



[ CorrespondingClosingBracket i phrasenamearray k;
  ! given a .opt(, find the corresponding '.)'
  ! WARNING do not nest .opt, it's too big of a pain to deal with
  !   so this routine is actually quite simple
  if (phrasenamearray-->i ~= '.opt(') {print "Error! This is a bug!"; return -1;}
  k=i; while (phrasenamearray-->k ~= '.)' && k < 32) {k++;}
  return k;
];

! TODO : improve this to take care of 'opt thing or thing', which should work

[ IsThereAnythingNonOptional i j phrasenamearray k token;
  ! Checks if the portion of the phrase_name between the ith and the jth word (included) have something that is non-optional
  k = i;
  while (k<=j)
  {
	token = phrasenamearray-->k;
	switch(token)
	{
	  '.or': k = k+2;
	  		! "X or Y": X, Y are either both optional or both non-opt; if we're here, X must be optional (otherwise it'd have returned)
	  '.opt': k = k+2;
	  '.opt(': k = CorrespondingClosingBracket(k, phrasenamearray) +1;   ! ignore all the .opt( ... .)
	   '.rest': return false;     ! if we're at this point there was nothing optional before and there's nothing after
	   './': return false;
	   default: return true;
	}
  }
  return false;
];

! Otherwise, too many arguments in parseonephrasename
Global wn_when_entered_the_opt=0;
Global acc_when_entered_the_opt=0;
Global read_when_entered_the_opt=0;

[ ParseOnePhraseName nbwordstocheck phrasenamearray wn_when_entered max i nbwordsininput n acc read optional phrasenametoken it_just_failed j ;

    if (nbwordstocheck == 0) {return 0;}

    wn = wn_when_entered;
    nbwordsininput = tokenCount();
    !print "Words in input : ", nbwordsininput, "^";
    n = 0;  ! Real score

    optional = 0;   ! if 1, we're looking at one optional word, if 2 it's a sentence
    it_just_failed = -1;   ! -1: read=1; 1: read=0 happened at the last iteration; 0: read=0 happened a long time ago

    ! If read=1 keep reading ; if it's 0, don't read until another sentence starts (and then read=1)
    read = 1;
    read_when_entered_the_opt=0;
    acc_when_entered_the_opt=0;

    for (i=0: i<nbwordstocheck: i++)
    {
      phrasenametoken = phrasenamearray-->i;

      !print "i at the very beginning: ", i, "^";
	  !print "Wn : ", wn, "^";
      !print "Word in wn : ";
      !  if (Word(wn)) {print (address) Word(wn);} else {print "0";}
      !  print " ; word in phrase_name : ";
      !  if (phrasenametoken) {print (address) phrasenametoken;} else {print "0";}
      !  print "^";
      !print "Score : n=", n, ", acc=", acc, "^";
      !print "Read = ", read, "^";


      ! If it's the end of a phrase, reset everything
      if ( phrasenametoken == './')
      {
		!print "Slash found : saved!^";
        if (read==1) { ! save the score
          if (acc>n) {n=acc;}
        }
        wn = wn_when_entered; ! start from the beginning again
        acc = 0;
        read=1; it_just_failed=-1;
        optional=0;
        continue;
      }
      ! If it's the end of an optional phrase: if it was ok don't do anything, if not go back to where you were before '.opt('
      if ( phrasenametoken == '.)')
      {
        !print "End of the optional sentence!^";
        if (read==0) { ! go back to the bookmark and reset the score
          wn = wn_when_entered_the_opt;
          acc = acc_when_entered_the_opt;
	    }
	    optional=0; wn_when_entered_the_opt=0; acc_when_entered_the_opt = 0;
	    read=read_when_entered_the_opt;
	    continue;
	  }

      ! If it's a or: if everything was fine, skip the word and the or, if not, consent to look at the next one
      if ( phrasenametoken == '.or' && it_just_failed ~= 0)
      {
		if (read == 0)  ! fine, we'll compare the word we just threw out to this one
		{ if (i<nbwordstocheck-1)
		  { i++;
		    phrasenametoken = phrasenamearray-->i;
		    read=1; it_just_failed=-1;
		  } else {continue;}
		  !print "We didn't recognize the previous but maybe it's this one: Word in wn : ", (address) Word(wn), " ; word in phrase_name : ", (address) phrasenametoken, "^";
		}
		else {
		  i=i+1; ! we continue, so it'll actually skip 2 words, the '.or' and the word itself
		  !print "We recognized one already, so no thank you! Word in wn : ", (address) Word(wn), " ; word in phrase_name : ", (address) phrasenametoken, "^";
		  continue;
	    }

	  }


      !  What if the player didn't type this many words? We process the rest of the phrase_name by hand
      if (wn > nbwordsininput)
      {
		!print "The player didn't type that many words: let's see if the phrase_name was done^";


		! What if we're inside a bracket?
		!    if the bracket was essentially done, it's all good
		!    if there was non-optional stuff, fail
		!    in all cases let the next loop deal with the end of the bracket, you'll fall back here anyway
	    if (optional == 2)
		{
		  ! Find the original .opt(
	      j = i; while (phrasenamearray-->j ~= '.opt(') {j--; }
		  ! Look into the whole bracket: was anything missing from the input?
		  if (IsThereAnythingNonOptional(i, CorrespondingClosingBracket(j, phrasenamearray)-1, phrasenamearray) == true)
		  { read = 0; }
		  i = CorrespondingClosingBracket(j, phrasenamearray);
		  i--; continue;                         ! let the beginning of next loop deal with the closing bracket
		 }

        ! If we're not inside a bracket
        !   Find the last token of the sentence
        j=i; while (phrasenamearray-->j ~= './' && j < nbwordstocheck) {j++;} j--;
        !   Check if the sentence is acceptable like that, ie it was basically done
        if (IsThereAnythingNonOptional(i,j, phrasenamearray) == true)
        { read=0;}
        i = j; continue;   ! the next loop with i+1 will land on the './' or outside the array, so the mail loop will break
      }


      ! Maybe we're not supposed to read
      if (read == 0)
      { !print "Skipping (read=0)^";
        it_just_failed=0;
        continue; ! we want as many words *in a row* as we can
      }


      ! Dealing with our special tokens
      switch( phrasenametoken )
      {
		  '.rest': ! The rest of the sentence is optional, so our current acc score is a score we should save
                   !print "Rest found : saved!^";
                   if (acc > n) {n = acc;}
                   continue;
          '.opt':  ! The next word is optional: if it's there, score, if not don't worry
                   optional=1;
                   if (i<nbwordstocheck-1) {i++; phrasenametoken = phrasenamearray-->i;} else {read=0; it_just_failed=0; continue;}
                   !print "Optdetected 1; Word in wn : ", (address) Word(wn), " ; word in phrase_name : ", (address) phrasenametoken, "^";
          '.opt(': ! We attempt to identify a group of optional words: we need a bookmark to come back if it failed
                   wn_when_entered_the_opt = wn;
                   read_when_entered_the_opt = read;
                   acc_when_entered_the_opt = acc;
                   optional=2;
                   if (i<nbwordstocheck-1) {i++; phrasenametoken = phrasenamearray-->i;} else {read=0; it_just_failed=0; continue;}
                   !print "Optdetected 2; Word in wn : ", (address) Word(wn), " ; word in phrase_name : ", (address) phrasenametoken, "^";
      }


      ! Dealing with words

      !  We are looking at an actual word from the player
      !    Is the word we're looking at in phrase_name a single letter that Inform is confusing for a character constant?
      if ( phrasenametoken <= 'z' && phrasenametoken >= 'a')
      {
		  phrasenamearray-->i = ReplaceSingleLetter(phrasenametoken);
		  phrasenametoken = phrasenamearray-->i;
	  }
	  !    Is the word we're looking at the token '.name' ?
	  if ( phrasenametoken == '.name')
	  {
		  if (WordInProperty(Word(wn), self, name)) {acc++; wn++; continue;}
		  else { read=0; it_just_failed=1; continue;}
	  }
	  !    Now compare the player's input to phrase_name
      if ( phrasenametoken == Word(wn))
      { ! We found a word
        !print "Found ! ", (address) Word(wn), "^";
	if (optional == 1) { optional = 0; } 	! if this was '.opt' 'goodword' 'tree', the word 'tree' is not optional
        acc++;
        wn++; continue;
      }
      else {
		! this is not in our phrase : if the word was optional, let it go, if not don't read the rest
        if (optional == 1)
        { !print "No worries, it was an optional word^";
		  optional = 0; continue;
		} else {  ! optional = 0 (ie mandatory) or optional = 2 (failed to recognize the full optional sentence)
		  read = 0;
		  it_just_failed=1;
		  continue;
	    }
      }

    }
    ! Simulate final '/'
    if (read==1) { if (acc > n) {n = acc;}}
    ! Update max score
    if (n>max) {max=n;}
    ! Reset global variables
    wn_when_entered_the_opt=0;
	acc_when_entered_the_opt=0;
    read_when_entered_the_opt=0;

    return max;
];


[ ComplicatedParsing max wn_when_entered sc ;
  !print "Called by : ", (the) self, "^";

  max = 0;
  wn_when_entered = wn;

  ! Regular parse_name first
  !    This is to make it easier, for instance if you want all your objects to call this routine
  !    You probably shouldn't put too many names in name if you're using ComplicatedParsing
  !    If you want to disable that, it's just the next line
  while (WordInProperty(NextWord(), self, name)) max++;


  ! Complicated one
  if (self provides phrase_name)
  { sc = ParseOnePhraseName( (self.#phrase_name)/WORDSIZE, self.&phrase_name, wn_when_entered);
    if (max < sc) {max = sc;}
  }
  if (self provides phrase_name2)
  { sc = ParseOnePhraseName( (self.#phrase_name2)/WORDSIZE, self.&phrase_name2, wn_when_entered);
      if (max < sc) {max = sc;}
  }

  !print "Final score : ", max, "^";
  return max;
];