! Multiple Choice, an Inform 6 extension by Hugo Labrande
! This extension is in the public domain; shoutouts are always appreciated.

! Usage: include this file between 'Include "parser"' and 'Include "verblib"'

! This extension can use Vorple effects; for this, your includes will look something like
! Include "vorple.h";
! Include "parser";
! Include "vorple-hyperlinks.h";
! Include "multiplechoice.h";
! Include "verblib";


!==================== Node class
! Put the text in the attribute "text", the first choice in choice1 and its result in w_to (choice2 = s_to, choice3 = e_to, choice4 = ne_to)
! For instance:
! Node beginning
!    with text "You are in a castle; there is a red door and a green door.",
!    choice1 "Open the red door",
!    w_to reddoor,
!    choice2 "Open the green door",
!    s_to [; if (key in player) { print "You open the green door with the key."; return greendoor;} else {"It's locked.";} ];

Property text;
Property choice1;
Property choice2;
Property choice3;
Property choice4;

#IfDef VORPLE_LIBRARY;
[ MyPrintOrRun obj prop flag n    dontShow silent id cmd ;
#IfNot;
[ MyPrintOrRun obj prop flag n ;
#Endif;
    if (obj provides prop) {
#IfDef VORPLE_LIBRARY;
        if (n > 0) VorpleOpenHTMLTag("span", "choices");
#EndIf;
        ! If it's the description
        if (n == 0) { PrintOrRun(obj, prop, flag); rtrue; }
        ! If it's a choice
        print "", n, ") ";
#IfDef VORPLE_HYPERLINKS;
        ! I recommend hiding the prompt and the commands for a better effect; see the vorple library for these effects
        if (isVorpleSupported()) {
            dontShow = 0;   ! put this at DONT_SHOW_COMMAND to hide them
                            ! I also recommend hiding the vorple prompt
            if (dontShow == DONT_SHOW_COMMAND) {
                silent = "true";
            } else { silent = "false"; }
            id = UniqueIdentifier();
            VorplePlaceElement("a", BuildCommand("vorple-link vorple-commandlink link-", id, " "));
            PrintOrRun(obj, prop, flag);
            VorpleCloseHTMLTag();
            switch(n) {
                1: cmd = "1";
                2: cmd = "2";
                3: cmd = "3";
                4: cmd = "4";
            }
            VorpleExecuteJavaScriptCommand(BuildCommand("$('a.link-", id, "').attr('href', '#').attr('data-command', '", cmd, "').attr('data-silent', '", silent, "')"));
        } else { PrintOrRun(obj, prop, flag); }
#IfNot;
        PrintOrRun(obj, prop, flag);
#Endif;
#IfDef VORPLE_LIBRARY;
        VorpleCloseHTMLTag();
#EndIf;
    }
];

Class Node
    with short_name "",
    description [;
        ! print the text
        MyPrintOrRun(self, text, 0); print "^";
        ! print the choices
        MyPrintOrRun(self, choice1, 0, 1);
        MyPrintOrRun(self, choice2, 0, 2);
        MyPrintOrRun(self, choice3, 0, 3);
        MyPrintOrRun(self, choice4, 0, 4);
        rtrue;
    ],
    cant_go [; print "Wrong choice number, please try again."; <<Look>>; ],
has light;




!==================== Grammar stuff

! add a prefix in front of every command, to short-circuit the library
! You can delete the grammar lines and the default messages if you want to save some space

[ BeforeParsing pos ;
   #Ifdef TARGET_ZCODE;
       pos = parse->5;
   #Ifnot; ! TARGET_GLULX
       pos = parse-->3;
   #Endif; ! TARGET_
   LTI_Insert(pos, 'c');
   LTI_Insert(pos+1, 'h');
   LTI_Insert(pos+2, 'o');
   LTI_Insert(pos+3, 'i');
   LTI_Insert(pos+4, 'c');
   LTI_Insert(pos+5, 'e');
   LTI_Insert(pos+6, ' ');
   Tokenise__(buffer,parse);
];


Constant CLEAR_SCREEN_BETWEEN_CHOICES 1;
[ Clear ;
    if (CLEAR_SCREEN_BETWEEN_CHOICES) { ClearScreen(); }
];

[ choiceNumberProcessingSub ;
    if (noun == 1) { Clear(); <<Go w_obj>>; }
    if (noun == 2) { Clear(); <<Go s_obj>>; }
    if (noun == 3) { Clear(); <<Go e_obj>>; }
    if (noun == 4) { Clear(); <<Go ne_obj>>; }
    print "Wrong choice number, please try again."; <<Look>>;
];

Verb 'choice'
* number        ->choiceNumberProcessing;
