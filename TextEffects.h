!---------------------------------------------------------------------------
!              TextEffects
!
!        by    Hugo Labrande   ( pr�nom at hlabrande point fr)
!
!    Version : 20/08/2015
!
!   (License: Public domain)
!
!============================================================
!
!    This extension has all the text effects I could think of:
!      - bold, italics, fixed-width and reverse; colors; and any combination of this
!            ex: WriteText("my text", EXT_BOLD|ITALICS|GREEN);
!      - all caps, alternate caps and non-caps (every other letter or every n letter),
!        define a probability for a letter to be a capital
!            ex: AllCaps("My text");
!                AlternatingCaps("My text");
!                AlternatingCaps("My other text", 3);  ! 1 out of 3 is capital
!                RandomCaps("My random text", 40); !40% chance of capital
!      - centered text on multiple lines with multiple styles
!            ex: BeginCenteredText(20);      ! margin = 10% on left, 10% on right
!                CenteredText("My text :");
!                WriteCenteredText("My text in bold", BOLD);
!                CenteredText("+ My test in yellow^And some more next line", YELLOW);
!                EndCenteredText();
!        with a caveat: since no one knows which font is used by the interpreter,
!           the effect will be imperfect except on fixed-width fonts
!       - wait x tenths of seconds or a keystroke and display text
!            ex: AfterWaiting("My text", 10);
!       - wait x tenths of seconds regardless of keystrokes and display text
!            ex: AfterStrictWaiting("My text", 10);
!       - slow printing of text (0 = instantaneous, 1 = slow, 10 = fast, -1 = really slow)
!            ex: WriteTextSlowly("Slooooow teeeext", 5);
!       - 'forced input': display one letter for each keystroke. Gets its name from
!         the fact it can simulate a command line input. First used in "Shrapnel".
!            ex: ForcedInput("kiss romeo");
!
!    And it's bi-platform!... Well, as much as I could: using coloured text in Glulx
!  is just not possible, because the way the styles are set up (no way to change
!  styles dynamically, and only 2 user-defined styles that I leave for authors), and
!  bold and italics might be displayed as something different in some interpreters.
!    And it detects whether the interpreter supports color/time effects - crucial,
!  since Parchment doesn't support time, you wouldn't want your game to be unplayable
!  online because you wanted a typewriter effect.
!    All this comes for the low, low cost of 4K added to your game.
!
!
!---------------------------------------------------------------------------
!
!   Use : add
!          Include "EffetsDeTexte";
!    after Include "Grammar" in your code.
!
!    Some complex effects need an array to perform some operations; this array
!   is of size 1000 by default. Break your text in several strings or modify
!   the size of the array (and hence the size of your game).
!
!---------------------------------------------------------------------------

System_file;


Constant COLOR;   ! for Inform 6/12



! When we use print_to_array, '^' is replaced by a new line character
!       but Zcode uses 'carriage return' ('\r'), which was the norm on old micros (ZX, C64, etc) and Mac before OSX
!       and Glulx uses 'line feed' ('\n'), like Unix.
!   There are lots of norms and conventions out there: Windows uses '\r\n', which means
!       Unix files sometimes appear in Windows as one big line
!   Anyway, it's just a big standardisation debate
#Ifdef TARGET_ZCODE;
Constant NEW_LINE_CHAR = 13;     ! '\r'
#Ifnot; ! TARGET_GLULX
Constant NEW_LINE_CHAR = 10;     ! '\n'
#Endif;





! Workspace array
Constant WORKSPACE_ARRAY_SIZE = 1000;
Array workspace buffer WORKSPACE_ARRAY_SIZE;
[ CleanWorkspace i; for (i=0 : i<workspace-->0 : i++) workspace->(WORDSIZE+i) = 0; ];

! Workspace array for centering
Constant WORKSPACE_ARRAY_SIZE_CENTERING = 400; ! no more than 400 chars per line, seems logical
Array centered_line buffer WORKSPACE_ARRAY_SIZE_CENTERING;
Array styles_centered_line buffer WORKSPACE_ARRAY_SIZE_CENTERING;
[ CleanCenterdLine i;
  for (i=0 : i<centered_line-->0 : i++) {
	  centered_line->(WORDSIZE+i) = 0;
	  styles_centered_line->(WORDSIZE+i) = 0;
  }
];
Array workspace_centre_input buffer WORKSPACE_ARRAY_SIZE; ! the array that catches what the author says






!----------------------------------------------------------------------------
!                                STYLE DU TEXTE



!=====================================
!    Constants

! Style constants: the same as Inform's
Constant EXT_REVERSE =		$$00000001;
Constant EXT_BOLD = 		$$00000010;
Constant ITALICS =		$$00000100;
Constant FIXEDWIDTH =   $$00001000;
Constant F_W =			$$00001000;
Constant FIXED_WIDTH =  $$00001000;
Constant BLACK =        $$00010000;
! Color constants: follow the RGB addition rules except black
Constant RED =          $$00100000;
Constant GREEN =        $$01000000;
Constant BLUE =         $$10000000;
Constant YELLOW =       $$01100000;
Constant PURPLE =       $$10100000;
Constant VIOLET =       $$10100000;
Constant MAGENTA =      $$10100000;
Constant CYAN =         $$11000000;
Constant WHITE =        $$11100000;


!

!=====================================
! Does the interpreter support...

[ IsColourSupported ;
#Ifdef TARGET_ZCODE;
  return ((0->1) & 1 ~= 0);
#Ifnot; ! TARGET_GLULX
  return true;              ! Technically, color is always supported; in practice
                            !   we can't use it
                            ! Should we then say false?
#Endif;
];

[ IsTimeSupported ;
#Ifdef TARGET_ZCODE;
  return ((0->1) & 128 ~= 0);
#Ifnot; ! TARGET_GLULX
  return glk_gestalt(gestalt_Timer, 0);
#Endif;
];


!=====================================
!    Display (generic routine)
!   Needs 2 arguments, text and its style

[ SetStyle style sty16 ;

  ! We start from default so that arg2=0 corresponds to roman
  #Ifdef TARGET_ZCODE;
    @set_text_style 0;
    if (IsColourSupported() == true) {
      SetColour(1, 1);
    }
  #Ifnot; ! TARGET_GLULX
    glk_set_style(style_Normal);
  #Endif;

  ! Non-color style
  sty16 = style % 16;
  #Ifdef TARGET_ZCODE;
  if (standard_interpreter > $100) {
	! standard 1.1 and above implement @set_text_style = 5 and such
	! should we do it ourselves for quickness? not sure
    @set_text_style sty16;
  }
  else {
    ! cf http://www.wolldingwacht.de/if/z-spec10.pdf p162:
    if (sty16 & $$1000) {@set_text_style 8;}
    if (sty16 & $$0100) {@set_text_style 4;}
    if (sty16 & $$0010) {@set_text_style 2;}
    if (sty16 & $$0001) {@set_text_style 1;}
  }
  #Ifnot; ! TARGET_GLULX

  ! Glulx terps never agree. Here are the default values for some:

  !					Gargoyle		WinGit			WinGlulxe		Quixe
  !	Emphasized		italics			Bold			Bold			Italics
  !	Preformatted	Fixed-width		Fixed-width		Fixed-width		Fixed-width
  !	Header			Bold			Bold (larger)	Bold (larger?)	Bold (larger)
  !	Subheader		Bold			Bold (larger?)	Bold			Bold (larger?)
  !	Alert			Bold + italics	Bold (larger?)	Bold			Bold
  !	Note			Italics			Italics			Italics			Italics
  !	BlockQuote		Normal			Normal			Normal			Normal fond jaune
  !	Input			Green			Bold			Bold			Dark red

    ! Everyone agrees with Note = Italics and Preformatted = Fixed-width
    if (sty16 & $$1000) {glk_set_style(style_Preformatted);}
    if (sty16 & $$0100) {glk_set_style(style_Note);}
    ! Subheader is in bold, but sometimes is bigger; Alert is bold, but bold+italics in Gargoyle
    ! We pick subheader, because it's not bigger by much on terps for which it is
    if (sty16 & $$0010) {glk_set_style(style_Subheader);}
    ! For reverse, BlockQuote is the style that approximates it better
    !    (remember that reverse is mostly used to make quoteboxes and statuslines in Z-Machine)
    ! It's prettier on Quixe and does nothing for the rest, doesn't matter
    if (sty16 & $$0001) {glk_set_style(style_BlockQuote);}
  #Endif;

  ! Color
  if (IsColourSupported() == true) {
  #Ifdef TARGET_ZCODE;
	clr_on = 1;
	if (style >= 16) {
      sty16 = (style-sty16)/32+ 2;
      SetColour(sty16, 1);
    }
  #Ifnot; ! TARGET_GLULX
    ! Do nothing: can't display color without messing with special styles
  #Endif;
  }
];

[ Display str style;
  ! Careful again! In glulx this won't work; depends on defined styles and the terp
  !   We can't allow ourselves to change user styles

  SetStyle(style);
  print (string) str;
  SetStyle(0);

  return true;
];


[ DisplayText string style ;
  Display(string, style);
];



!=====================================
!        Caps
!   write text in all caps, or partially caps!
!   Length limited by the size of the workspace array


[ Caps str i c;
  CleanWorkspace();
  str.print_to_array(workspace, WORKSPACE_ARRAY_SIZE);
  for (i = 0: i<workspace-->0: i++) {
    c = workspace->(WORDSIZE+i);
    c = UpperCase(c);
    print (char) c;
  }
  return true;
];

[ CapsLock str ;
  return Caps(str);
];
[ AllCaps str ;
  return Caps(str);
];


! Alternate caps and lowercase, lIkE TeEnS Do
!      Optional second argument: optional, to say "write a capital letter every n letters"
[ AlternateCaps str n i j c;
  CleanWorkspace();
  str.print_to_array(workspace, WORKSPACE_ARRAY_SIZE);
  if (n<=0) { n=2; }
  j=1;
  for (i = 0: i<workspace-->0: i++) {
    c = workspace->(WORDSIZE+i);
	if (j == n) { print (char) UpperCase(c); j=1; }
	else { print (char) c; j++; }
  }
  return true;
];

! Random caps: probability x% that a letter is a capital letter
[ RandomCaps str p i c;
  CleanWorkspace();
  str.print_to_array(workspace, WORKSPACE_ARRAY_SIZE);
  if (p<0 || p>100) { p=50;}
  for (i = 0: i<workspace-->0: i++) {
    c = workspace->(WORDSIZE+i);
	if (random(100) <= p) { print (char) UpperCase(c); }
	else { print (char) c; }
  }
  return true;
];



!=====================================
!    Centered text

! Begin by calling "StartCentering(percent)", the argument being the percentage for margins (20% = 10% left margin
!   and 10% right margin)
! Then write whatever you want with calls like "Centered("My string", EXT_BOLD)"
!  If you want to add more spaces so that it looks more centered (careful! it won't work on all terps!), modify
!    the global variable ext_extra_spaces; for instance
!      StartCentering(20);
!      Centered("First line of the text");
!      ext_extra_spaces = 5;
!      Centere("This line is longer, we add spaces to compensate and center better");
!      ext_extra_spaces = 0;
!      EndCentering();
!    This centering effect will always be imperfect because the player can change fonts.
! Try not to mix this with other statements displaying things, or always end with ^, or some stuff might not print right.
! When you're done, call "EndCentering()"

Global ext_centered = 0;
Global ext_centered_width = 0;
Global ext_centered_alreadyprinted = 0;
Global ext_extra_spaces = 0;

[ DisplayCenteredLine len current_style i;
  SetStyle(FIXED_WIDTH);
  Print__Spaces( (ScreenWidth()-len)/2 + ext_extra_spaces);
  current_style = FIXED_WIDTH;
  for (i=0: i<len: i++) {
	  if (current_style ~= styles_centered_line->(WORDSIZE+i)){
		  current_style = styles_centered_line->(WORDSIZE+i);
		  SetStyle(current_style);
	  }
	  print (char) centered_line->(WORDSIZE+i);
  }
  print "^";
  CleanCenterdLine();
  SetStyle(0);
];

[ StartCentering percent w;
  w = ScreenWidth();
  if (percent < 0 || percent > 100) {ext_centered_width = w;}
  else {ext_centered_width = (w*(100-percent))/100;}
  CleanCenterdLine();
  ext_centered = 1;
  ext_centered_alreadyprinted = 0;
];

[ EndCentering ;
  DisplayCenteredLine(ext_centered_alreadyprinted);
  ext_centered = 0; ext_centered_width = 0; ext_centered_alreadyprinted = 0;
];

[ Centered string style rem i c pos;
  if (ext_centered) {
	  string.print_to_array(workspace_centre_input, WORKSPACE_ARRAY_SIZE);
	  rem = workspace_centre_input-->0;
	  pos = 0;
	  i = ext_centered_alreadyprinted;
	  while (rem ~=0) {
		  ! Copies as many chars as it can, i.e. as long as the line isn't full or we don't hit a "^"
		  c = workspace_centre_input->(WORDSIZE+pos); pos++; rem--;
          if (c == NEW_LINE_CHAR || i == ext_centered_width) {
			  ! display the line
			  DisplayCenteredLine(i); i = 0;
			  ! if it's not "^" we need to add that char to the beginning of next line
			  if (c ~= NEW_LINE_CHAR) {
				  centered_line->(WORDSIZE+i) = c;
				  styles_centered_line->(WORDSIZE+i) = style;
				  i = 1;
			  }
		  }
		  else {
			  ! add the char with the right style
			  centered_line->(WORDSIZE+i) = c;
			  styles_centered_line->(WORDSIZE+i) = style;
			  i++;
		  }

	  }
	  ext_centered_alreadyprinted = i;
  }
  else { print string; }
];


!----------------------------------------------------------------------------
!                               TIME EFFECTS

!=====================================
!    AfterWait and AfterStrictWait
!   (display something after some time)

[ AfterWait str tenths ;
  if (IsTimeSupported() == true) {
    KeyDelay(tenths);
  }
  ! If the terp doesn't do time effects, just display the text
  print (string) str;
  return true;
];

[ AfterStrictWait str tenths v;
  ! You can always skip a wait by pressing a key in Z-Code
  ! Instead of one big wait, we wait a lot of times 1/10th of a second, so when
  !   the player pushes a key only one of those is shortened. Of course, if the
  !   player keeps pressing the key, the wait will be shorter (how much depends on
  !   the terp) but you can't do anything about that.
  ! This trick isn't mine, but comes from the "Markup" extension
  if (IsTimeSupported() == true) {
    for (v=0: v<tenths: v++){
      KeyDelay(1);
    }
  }
  ! If the terp doesn't do time effects, just display the text
  print (string) str;
  return true;
];


!=====================================
!    Scrolling(text, speed)
!   (10 = quick, 1 = slow, 0 = instantaneous, -1 very slow)


[ Scrolling txt speed i c j packet delay;
  ! Formula : (n^2+1)/2 letters per each tenth of a second (1, 2, 5, 8, 13, 18, 25, 32, 41, 50)
  ! -1, -2, ... : n^2/4+2 tenths of seconds per letter (2, 3, 4, 6, 8, 11, 14, 18, 22, 27)

  if (speed == 0 || IsTimeSupported() == false) {print (string) txt; return true;}
  CleanWorkspace();
  txt.print_to_array(workspace, WORKSPACE_ARRAY_SIZE);
  if (speed < 0) {
	packet = 1; delay = (speed*speed)/4+2;
  } else {
    packet = (speed*speed+1)/2; delay = 1;
  }
  j = 0;
  for (i=0: i<workspace-->0: i++)
  {
	  c = workspace->(WORDSIZE+i);
	  j++; if (j==packet) {
		j=0; KeyDelay(delay);
	  }
	  print (char) c;
  }
  return true;
];

[ PrintSlow txt speed;
  Scrolling(txt, speed);
];
[ SlowDisplay txt speed;
  Scrolling(txt, speed);
];
[ DisplaySlowly txt speed ;
  Scrolling(txt, speed);
];


!=====================================
!    ForcedInput

! Each time the player presses a key, one more letter is displaying
! Useful if you want to make it look like the player can write their
!   own command, but they actually can't
! No need for the terp to support time effects here

[ ForcedInput str i;
  CleanWorkspace();
  str.print_to_array(workspace, WORKSPACE_ARRAY_SIZE);
  for (i = 0: i<workspace-->0: i++) {
    KeyCharPrimitive();
    print (char) workspace->(WORDSIZE+i);
  }
  KeyCharPrimitive();         ! To simulate pressing "Return"
];

[ FakePrompt str;
  ForcedInput(str);
];

