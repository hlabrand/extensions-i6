!---------------------------------------------------------------------------
!              OneWordParsing
!
!        par   Hugo Labrande
!
! Version du 05/08/2015
!
! (Licence : Domaine public)
!
!---------------------------------------------------------------------------
!
!    Cette extension vous permet de reconna�tre des commandes faites d'un
!  seul mot, sans verbe, comme dans 'Blue Lacuna' ou 'Walker and Silhouette'.
!  Les commandes accept�es sont celles qui correspondent � un objet, et par
!  d�faut l'action produite sera de lancer la routine de description de
!  l'objet.
!    Il y a deux modes, activ�s suivant si les constantes suivantes sont
!  d�finies:
!       - DONT_ALLOW_VERBS : l'extension d�sactive tous les
!         verbes de la biblioth�que standard, sauf les directions,
!         l'inventaire et verbes 'm�ta' ('sauver', 'charger', etc.), et votre
!         routine MyParserError est appel�e quand la commande n'est pas reconnue;
!       - ALLOW_VERBS : vous permettez au joueur de
!         toujours pouvoir utiliser les verbes, mais aussi de pouvoir taper
!         le nom d'un objet pour avoir sa description.
!    Cette extension utilise deux fonctions, qui sont par d�faut vides:
!  BeforeParsing, et UnknownVerb (ou MyUnknownVerb pour la biblioth�que
!  francophone). Si vous les avez d�j� d�finies, changez le nom des routines
!  de cette extension et appelez-les depuis vos routines.
!    Et c'est compatible avec Glulx !
!
!    This extension enables you to parse commands made of one word, with no
!  verb, as in 'Blue Lacuna' or 'Walker and Silhouette'. The commands that
!  are accepted are the ones that correspond to an object, and by default
!  the resulting action would be to start the description routine of an
!  object.
!    There are two modes, activated depending on whether the following
!  constants are defined:
!       - DONT_ALLOW_VERBS: the extension deactivates all the
!         verbs from the standard library, except directions, inventory
!         and meta verbs ('save','restore', etc.), and your routine MyParserError
!         is called whenever the player types something wrong;
!       - ALLOW_VERBS: allow the player to still be able to use
!         verbs, but also to be able to type the name of an object to get
!         its description.
!    This extension uses two functions which are stubs by default:
!  BeforeParsing and UnknownVerb (or MyUnknownVerb for the French-speaking
!  library). If you already defined those, change the names of the routines
!  in this extension and call them from your own routines.
!    And yes, this is Glulx-compatible!
!
!---------------------------------------------------------------------------
!
!    Utilisation : rajoutez                 Usage: add
!          Include "OneWordParsing";             Include "OneWordParsing";
!    n'importe o� apr�s                     any time after
!          Include "VerbLib";                    Include "VerbLib";
!    dans votre code, et d�finissez         in your code, and define the
!    la routine MyParserError et la         MyParserError routine, as well
!    constante DONT_ALLOW_VERBS ou          as the constant DONT_ALLOW_VERBS
!    ALLOW_VERBS avant d'inclure            or ALLOW_VERBS before including
!    cette extension.                       this extension.
!
!---------------------------------------------------------------------------


! TODO: support a few more debug verbs: gonear noun, goto number, scope noun,
!               showobj number / multi, showverb special, trace number,
!               abstract noun to noun, purloin multi, tree noun



! A few routines we need

#IfNDef MyParserError;          ! Define your own MyParserError to guide the player!
[ MyParserError ;
#Ifdef DONT_ALLOW_VERBS;
	L__M(##Miscellany, 27);     ! I didn't understand that sentence
#Endif;
#Ifdef ALLOW_VERBS;
	L__M(##Miscellany, 38);     ! I don't recognise that verb
#Endif;
    return true;
];
#Endif;




[ AnythingToken ;
while (NextWordStopped() ~= -1) ; return GPR_PREPOSITION;
];

[ OneWordFailSub;
    MyParserError(); return true;
];














!===============================================
!     Allow all the other verbs

#Ifdef ALLOW_VERBS;
   ! Recognize one-word commands on top of verbs
   ! This is done by prefacing the commands for which the verb was not recognized
   ! Orders should still work once the bug 1498 is fixed in I6

#Ifdef LIBRARY_FRENCH;
[ MyUnknownVerb ;
#Ifnot;
[ UnknownVerb ;
#Endif;
	verb_wordnum = 0;
	return '.verb';         ! the period ensures the player can't type this
];

[ PrintVerb v;
        if (v == '.verb') {
			#Ifdef LIBRARY_FRENCH;
			print "interagir avec";
			#Endif;
			#Ifdef LIBRARY_ENGLISH;
			print "interact with";
			#Endif;
			rtrue;
		}
        rfalse;
];

Verb '.verb'
* noun 				->Examine
* AnythingToken		->OneWordFail;

#Endif;







!================================================
! No verbs allowed, except inventory, meta verbs and directions

#Ifdef DONT_ALLOW_VERBS;
   ! We don't allow the use of verbs
   ! This is done by prefacing all the commands by 'verb'
[ BeforeParsing pos ;
   #Ifdef TARGET_ZCODE;
       pos = parse->5;
   #Ifnot; ! TARGET_GLULX
       pos = parse-->3;
   #Endif; ! TARGET_
   LTI_Insert(pos, 'v');
   LTI_Insert(pos+1, 'e');
   LTI_Insert(pos+2, 'r');
   LTI_Insert(pos+3, 'b');
   LTI_Insert(pos+4, 'l');
   LTI_Insert(pos+5, 'e');
   LTI_Insert(pos+6, 's');
   LTI_Insert(pos+7, 's');
   LTI_Insert(pos+8, ' ');
   Tokenise__(buffer,parse);

];

[ isInventory ;
#Ifdef LIBRARY_FRENCH;
	if (NextWord() == 'inventaire' or 'inv' or 'i//' or 'inventoire') {
#Endif;
#Ifdef LIBRARY_ENGLISH;
    if (NextWord() == 'inventory' or 'inv' or 'i//') {
#Endif;
		return GPR_PREPOSITION;
	} else { return GPR_FAIL; }
];


[ SomeDir ; if (noun in compass) rtrue; rfalse; ];


Verb 'verbless'

! Standard directions for moving
* noun=SomeDir			->Go

! Inventory
* isInventory			->Inv;


!============ meta verbs
!    le joueur s'attend � ce que ces verbes soient pr�sents, il est fortement d�conseill� de les enlever!
!    those are expected by the player, and it is strongly advised NOT to remove them

Global meta_verb = 0;

[ metaVerbSub ;
	switch(meta_verb){
		1: <<LMode1>>;
		2: <<LMode2>>;
		3: <<LMode3>>;
		4: <<NotifyOn>>;
		5: <<NotifyOff>>;
		6: <<Pronouns>>;
		7: <<Quit>>;
		8: <<CommandsOn>>;
		9: <<CommandsOff>>;
		10: <<CommandsRead>>;
		11: <<Restart>>;
		12: <<Restore>>;
		13: <<Save>>;
		14: <<Score>>;
		15: <<FullScore>>;
		16: <<ScriptOn>>;
		17: <<ScriptOff>>;
		18: <<Verify>>;
		19: <<Version>>;
	}
#Ifdef LIBRARY_FRENCH;
	if (meta_verb == 20) { <<VersionFR>>; }
#Endif;
#Ifndef NO_PLACES;
	if (meta_verb == 21) { <<Objects>>; }
	if (meta_verb == 22) { <<Places>>; }
#Endif; ! NO_PLACES

#Ifdef DEBUG;
	switch(meta_verb) {
		23: <<ActionsOn>>;
		24: <<ActionsOff>>;
		25: <<ChangesOn>>;
		26: <<ChangesOff>>;
		27: <<GoNear>>;
		28: <<Goto>>;
		29: <<Predictable>>;
		30: <<RoutinesOn>>;
		31: <<RoutinesOff>>;
		32: <<TimersOn>>;
		33: <<TimersOff>>;
		35: <<Scope>>;
		36: <<Showobj>>;
		37: <<Showverb>>;
		38: <<TraceOn>>;
		39: <<TraceOff>>;
		40: <<TraceLevel>>;
		41: <<XAbstract>>;
		42: <<XPurloin>>;
		43: <<XTree>>;
	}
#Ifdef TARGET_GLULX;
	if (meta_verb == 34) {<<Glklist>>;}
#Endif;
#Ifdef LIBRARY_FRENCH;
	if (meta_verb == 44) {<<AccentsOn>>;}
	if (meta_verb == 45) {<<AccentsStrict>>;}
	if (meta_verb == 46) {<<AccentsOff>>;}
#Endif;
#Endif;

];

[ metaParsing w z;
	meta_verb = 0;
	w = NextWord();
#Ifdef LIBRARY_ENGLISH;
	switch(w) {
		'brief','normal': meta_verb = 1;
		'verbose','long': meta_verb = 2;
		'superbrief','short': meta_verb = 3;
		'notify': z = NextWord(); switch(z){
					'on': meta_verb = 4;
					-1:   meta_verb = 4;
					'off':meta_verb = 5;
				}
		'pronouns','nouns': meta_verb = 6;
		'quit', 'q//': meta_verb = 7;
		'recording': z = NextWord(); switch(z){
					'on': meta_verb = 8;
					-1:   meta_verb = 8;
					'off':meta_verb = 9;
				}
		'replay': meta_verb = 10;
		'restart': meta_verb = 11;
		'restore': meta_verb = 12;
		'save': meta_verb = 13;
		'score': meta_verb = 14;
		'fullscore','full': z = NextWord(); switch(z){
					'score': meta_verb = 15;
					-1:   meta_verb = 15;
				}
		'script','transcript': z = NextWord(); switch(z){
					'on': meta_verb = 16;
					-1: meta_verb = 16;
					'off': meta_verb = 17;
				}
		'noscript','unscript': meta_verb = 17;
		'verify': meta_verb = 18;
		'version': meta_verb = 19;
	}
	if (meta_verb ~=0) { meta=true; return GPR_PREPOSITION; }
#Ifndef NO_PLACES;
	if (w == 'objects') { meta_verb = 21; }
	if (w == 'places')  { meta_verb = 22; }
	if (meta_verb ~=0) { meta=true; return GPR_PREPOSITION; }
#Endif; ! NO_PLACES
#Endif;
#Ifdef LIBRARY_FRENCH;
	switch(w) {
		'brief','normal': meta_verb = 1;
		'verbose','long': meta_verb = 2;
		'superbrief','short': meta_verb = 3;
		'notify': z = NextWord(); switch(z){
					'on': meta_verb = 4;
					-1:   meta_verb = 4;
					'off':meta_verb = 5;
				}
		'pronouns','nouns': meta_verb = 6;
		'quit', 'q//': meta_verb = 7;
		'recording': z = NextWord(); switch(z){
					'on': meta_verb = 8;
					-1:   meta_verb = 8;
					'off':meta_verb = 9;
				}
		'replay': meta_verb = 10;
		'restart': meta_verb = 11;
		'restore': meta_verb = 12;
		'save': meta_verb = 13;
		'score': meta_verb = 14;
		'fullscore','full': z = NextWord(); switch(z){
					'score': meta_verb = 15;
					-1:   meta_verb = 15;
				}
		'script','transcript': z = NextWord(); switch(z){
					'on': meta_verb = 16;
					-1: meta_verb = 16;
					'off': meta_verb = 17;
				}
		'noscript','unscript': meta_verb = 17;
		'verify': meta_verb = 18;



		'mode': z = NextWord(); switch(z){
					'normal': meta_verb = 1;
					'long','bavard','verbeux': meta_verb = 2;
					'court': meta_verb = 3;
				}
		'notification': z = NextWord(); switch(z){
					'on': meta_verb = 4;
					-1:   meta_verb = 4;
					'off':meta_verb = 5;
				}
		'quitter': meta_verb = 7;
		'recommencer': meta_verb = 11;
		'charger': meta_verb = 12;
		'sauver','sauvegarder': meta_verb = 13;
		'score': z = NextWord(); if (z == 'detaille' or 'complet') { meta_verb = 15; }
		'log','transcription','transcrire': z = NextWord(); switch(z){
					'on': meta_verb = 16;
					-1: meta_verb = 16;
					'off': meta_verb = 17;
				}
		'nolog': meta_verb = 17;
		'verifier': meta_verb = 18;
		'version': z = NextWord(); switch(z){
					-1: meta_verb = 19;
					'francaise','fr','vf':   meta_verb = 20;
				}
		'vf': meta_verb = 20;
	}
	if (meta_verb ~=0) { meta=true; return GPR_PREPOSITION; }
#Ifndef NO_PLACES;
	if (w == 'objects' or 'objets') { meta_verb = 21; }
	if (w == 'places' or 'endroits')  { meta_verb = 22; }
	if (meta_verb ~=0) { meta=true; return GPR_PREPOSITION; }
#Endif; ! NO_PLACES
#Endif;

#Ifdef DEBUG;
	switch(w) {
		'actions': z = NextWord(); switch(z){
					'on': meta_verb = 23;
					-1: meta_verb = 23;
					'off': meta_verb = 24;
				}
		'changes': z = NextWord(); switch(z){
					'on': meta_verb = 25;
					-1: meta_verb = 25;
					'off': meta_verb = 26;
				}
		'gonear': print "GoNear unsupported, sorry!";     ! TODO: gonear noun
		'goto': print "Goto unsupported, sorry!";         ! TODO: goto number
		'random': meta_verb = 29;
		'routines','messages': z = NextWord(); switch(z){
					'on': meta_verb = 30;
					-1: meta_verb = 30;
					'off': meta_verb = 31;
				}
		'timers','daemons': z = NextWord(); switch(z){
					'on': meta_verb = 32;
					-1: meta_verb = 32;
					'off': meta_verb = 33;
				}
		'scope': meta_verb = 35;		! TODO: scope noun
		'showobj': meta_verb = 36;		! TODO: showobj number and multi
		'showverb': print "Showverb unsupported, sorry!";     ! TODO: showverb special
		'trace': z = NextWord(); switch(z){			!TODO: trace number
					'on': meta_verb = 38;
					-1: meta_verb = 38;
					'off': meta_verb = 39;
				}
		'abstract': print "Abstract unsupported, sorry!";     ! TODO: abstract noun to noun
		'purloin': print "Purloin unsupported, sorry!";     ! TODO: purloin multi
		'tree': meta_verb = 43;				!TODO: tree noun
	}
#Ifdef TARGET_GLULX;
	if (w == 'glklist') { meta_verb = 34; }
#Endif;
#Ifdef LIBRARY_FRENCH;
	if (w == 'accents') {
		z = NextWord(); switch(z){
			'on': meta_verb = 44;
			-1: meta_verb = 44;
			'strict': meta_verb = 45;
			'off': meta_verb = 46;
			}
	}
#Endif;
	if (meta_verb ~=0) { meta=true; return GPR_PREPOSITION; }
#Endif;

	if (meta_verb ~=0) { meta=true; return GPR_PREPOSITION; }
	return GPR_FAIL;
];

Extend 'verbless'
* metaParsing		->metaVerb;



!=================== the rest

Extend 'verbless'
! Objects
* noun				->Examine

! Something else
* AnythingToken		->OneWordFail;


#Endif;
