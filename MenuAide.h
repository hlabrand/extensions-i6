!---------------------------------------------------------------------------
!              MenuAide
!
!        par   Hugo Labrande
!
! Version du 28/07/2015
!
! (Licence : Domaine public)
!
!---------------------------------------------------------------------------
!
!    Cette extension automatise le menu d'aide que j'utilise dans la plupart
!  de mes jeux. Elle n�cessite l'extension que j'utilise d'habitude pour
!  les menus, DMenus.h, par Khelwood (qui n'est pas dans le domaine public,
!  elle).
!    Pour chaque jeu il faut d�finir son propre about, version et thankYou.
!
!    If you are making games in English, the menu will automatically display
!  in English; just know you need to include DMenus.h by Khelwood (not in the
!  public domain) and define your own about, version and thankYou routines.
!
!---------------------------------------------------------------------------
!
!    Utilisation : rajoutez
!          Include "MenuAide";
!    � la ligne juste apr�s
!          Include "FrenchG";
!      ou  Include "Grammar";
!    dans votre code.
!
!---------------------------------------------------------------------------

#Ifdef TARGET_ZCODE;
Zcharacter table + '@{2014}';
#Endif;

! La licence Creative Commons
[ licence ;
#Ifdef LIBRARY_FRENCH;
  style bold;
  print "Licence Creative Commons by-nc-nd^pour : @<< "; print (string) Story; print " @>>^par : Hugo Labrande";
  style roman;
  print "^^Paternit�-Pas d'Utilisation Commerciale-Pas de Modification 2.0 France^";
  print "^";
  print "Vous �tes libres : de reproduire, distribuer et communiquer cette cr�ation au public, selon les conditions suivantes :^";
  print "^";
  print "@{2014} Paternit�. Vous devez citer le nom de l'auteur original de la mani�re indiqu�e par l'auteur de l'@oeuvre ou le titulaire des droits qui vous conf�re cette autorisation (mais pas d'une mani�re qui sugg�rerait qu'ils vous soutiennent ou approuvent votre utilisation de l'@oeuvre). ^";
  print "^";
  print "@{2014} Pas d'Utilisation Commerciale. Vous n'avez pas le droit d'utiliser cette cr�ation � des fins commerciales. ^";
  print "^";
  print "@{2014} Pas de Modification. Vous n'avez pas le droit de modifier, de transformer ou d'adapter cette cr�ation. ^";
  print "^� chaque r�utilisation ou distribution de cette cr�ation, vous devez faire appara�tre clairement au public les conditions contractuelles de sa mise � disposition.^";
  print "^";
  print "Chacune de ces conditions peut �tre lev�e si vous obtenez l'autorisation du titulaire des droits sur cette @oeuvre.^";
  print "Rien dans ce contrat ne diminue ou ne restreint le droit moral de l'auteur ou des auteurs.^";
  print "^";
  print "Ce qui pr�c�de n'affecte en rien vos droits en tant qu'utilisateur (exceptions au droit d'auteur : copies r�serv�es � l'usage priv� du copiste, courtes citations, parodie...).^";
  print "^";
  print_ret "http://creativecommons.org/licenses/by-nc-nd/2.0/fr/legalcode";
#Endif;
#Ifdef LIBRARY_ENGLISH;
  style bold;
  print "License Creative Commons by-nc-nd ^for: ~"; print (string) Story; print "~ ^by: Hugo Labrande";
  style roman;
  print "^^Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)^";
  print "^";
  print "You are free to: Share -- copy and redistribute the material in any medium or format. The licensor cannot revoke these freedoms as long as you follow the license terms.";
  print "^Under the following terms:^";
  print "^";
  print "-- Attribution: You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use. ^";
  print "^";
  print "-- NonCommercial: You may not use the material for commercial purposes. ^";
  print "^";
  print "-- NoDerivatives: If you remix, transform, or build upon the material, you may not distribute the modified material. ^";
  print "^No additional restrictions: You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits. ^";
  print "^";
  print "^Notices:";
  print "^You do not have to comply with the license for elements of the material in the public domain or where your use is permitted by an applicable exception or limitation (fair use, citations, parodies, etc.).";
  print "^No warranties are given. The license may not give you all of the permissions necessary for your intended use. For example, other rights such as publicity, privacy, or moral rights may limit how you use the material.";
  print "^";
  print_ret "https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode";
#Endif; ! Licence
];

[ toBegin ;
#Ifdef LIBRARY_FRENCH;
  print " Vous ne savez pas comment jouer � une fiction interactive ? Voil� une petite aide pour d�buter.^^";
#Endif;
#Ifdef LIBRARY_ENGLISH;
  print " Perhaps you don't really know how to play interactive fiction? This should help you get started.^^";
#Endif;

#Ifdef LIBRARY_FRENCH;
  print " Dans une fiction interactive, aussi appel�e aventure textuelle, vous pouvez interagir avec le jeu en tapant des commandes au clavier. D�s que vous voyez un signe > appara�tre au d�but d'une ligne, cela signifie que c'est � vous, et vous pouvez taper votre commande ! Ces commandes doivent �tre de la forme :^";
  style underline; print "       verbe � l'infinitif + nom de l'objet concern� par l'action^"; style roman;
  print " Comme si le jeu vous demandait @<< Que voulez-vous faire maintenant ? @>>. Par exemple :^       prendre cl�^       ouvrir porti�re de la voiture^       parler � caissi�re^^";
#Endif;
#Ifdef LIBRARY_ENGLISH;
  print " In an interactive fiction game, also called text adventure, you can interact with the game by typing commands on your keyboard. Whenever you see a > sign show up on the screen at the beginning of a line, this means that it's your turn and you can type your command! Those commands must be of the form:^";
  style underline; print "       verb + name of the object the verb is applied to^"; style roman;
  print " It's as if the game was asking you ~What do you want to do now?~. For instance:^       get key^       open car door^       talk to cashier^^";
#Endif;

#Ifdef LIBRARY_FRENCH;
  print " La plupart des actions que vous pouvez accomplir sont relativement simples : examiner, prendre, pousser, ouvrir, etc. Dans les autres jeux vid�o, vous avez un bouton pour sauter, un pour tirer, et chaque bouton est en g�n�ral une action simple. Ici c'est un peu pareil : vous avez quelques actions simples � votre disposition, et vous vous en servez (en les combinant l'une apr�s l'autre) pour avancer dans le jeu, r�soudre les �nigmes et accomplir vos objectifs. Voici une petite liste pour d�marrer :^";
  style bold;
    print "         @{2014} D�placements :"; style roman; print " aller au nord, � l'ouest, au sud-ouest, en bas, dedans, dehors, etc. ; s'asseoir sur ..., entrer dans ..., etc.^"; style bold;
    print "         @{2014} Objets :"; style roman; print " regarder ..., prendre ..., ouvrir ..., pousser ..., poser ..., manger ..., mettre ..., d�verrouiller ...,  etc. La commande >inventaire vous montre les objets en votre possession.^"; style bold;
    print "         @{2014} Personnages :"; style roman; print " parler � ..., donner ... � ..., montrer ... � ..., etc.^"; style bold;
  print "         @{2014} Raccourcis :"; style roman; print " x ... pour examiner ou regarder un objet ; n vous permet d'aller au nord (et des raccourcis similaires sont reconnus pour les autres points cardinaux), i vous montre l'inventaire.^";
  print " Pour commencer, vous pouvez essayer d'examiner les objets mentionn�s dans la description du lieu, et essayer de les manipuler, de les prendre, etc. Puis essayez de vous d�placer vers des lieux environnants, en suivant les directions indiqu�es dans la description.^^";
#Endif;
#Ifdef LIBRARY_ENGLISH;
  print " Most of the actions you can accomplish are relatively simple: examine, take, push, open, etc. In the other kinds of video games, you have one button to jump, another one to shoot, and each button usually corresponds to a simple action. Here, it's kind of the same: you have a few simple actions available, and you use them, combining them one after the other, to move forward in the game, solve puzzles and achieve your goals. Here's a little list to start:^";
  style bold;
    print "         @{2014} Moving around:"; style roman; print " go north, go west, southwest, down, in, out, etc.; sit on ..., enter ..., etc.^"; style bold;
    print "         @{2014} Objects:"; style roman; print " examine ..., take ..., open ..., push ..., drop ..., eat ..., put ..., unlock ...,  etc. The >inventory command shows which objects you have in your possession.^"; style bold;
    print "         @{2014} Characters:"; style roman; print " talk to ..., give ... to ..., show ... to ..., etc.^"; style bold;
  print "         @{2014} Shortcuts:"; style roman; print " x ... to examine or look at an object; n to go north (and similar shortcuts exist for other compass directions), i will show the inventory.^";
  print " To begin, you could start by examining the objects mentioned in the location's description, and try manipulating them, taking them, etc. Then try to move in adjacent rooms or places, by following the directions written in the description.^^";
#Endif;

#Ifdef LIBRARY_FRENCH;
  print " Une grande attention � �t� port�e au fait que de nombreuses combinaisons @<< verbe + nom @>> sont possibles ; des verbes plus complexes ou plus pr�cis, ainsi que des synonymes, ont �t� pris en compte, le but �tant que vous ne vous sentiez pas bloqu�(e). Cela �tant, de temps en temps, le jeu vous dira quelque chose comme @<< Je ne comprends pas @>>, ce qui indique qu'il va falloir essayer de reformuler votre commande, ou v�rifier si l'objet auquel vous faites r�f�rence est bien dans le lieu o� vous vous trouvez !^ Si jamais vous avez l'impression que votre commande �tait parfaitement coh�rente et que c'est de ma faute si le jeu ne vous a pas compris, n'h�sitez pas � m'envoyer un mail pour me le dire ; vous me rendrez un service, � moi et aux futurs joueurs, en indiquant une commande � laquelle je n'avais pas pens� !^^";
  print " Parfois, dans le jeu, vous aurez fini de lire le texte, mais vous ne verrez pas de > ; cela signifie simplement que le jeu a fait une pause, pour vous laisser le temps de lire, et attend juste que vous appuyiez sur une touche pour reprendre. Essayez, pour voir !"; KeyCharPrimitive(); print " Voil� ! Le jeu affiche ensuite le reste du texte, jusqu'� la prochaine pause, ou jusqu'� ce que �a soit � vous de jouer !^^"; KeyCharPrimitive();
#Endif;
#Ifdef LIBRARY_ENGLISH;
  print " Great care has been taken to ensure that a lot of ~verb + noun~ combinations are recognized; more complex or more precise verbs, as well as synonyms, have been taken into account, the goal being that you do not feel stuck. That being said, from time to time, the game will tell you something like ~I don't understand~, which means that you are going to have to try to rephrase your command, or check if the object you are referring to is indeed in the location you are in!^If you happen to have the feeling that your command was perfectly coherent and it is my own fault if the game did not understand you, do not hesitate to send me an email to let me know; you will do me and future players of the game a great service by telling me about a command I hadn't thought of!^^";
  print " At some points in the game, you will be done reading the text, but will not see a > appear; this simply means that the game paused, to leave you time to read, and is just waiting for you to press a key to keep going. Try it now!"; KeyCharPrimitive(); print " There you go! The game then displays the rest of the text, until the next pause, or until it's your turn to play.^^"; KeyCharPrimitive();
#Endif;

#Ifdef LIBRARY_FRENCH;
  print " Un dernier conseil : � tout moment, vous pouvez taper @<< sauver @>> pour sauvegarder votre partie, @<< charger @>> pour charger une sauvegarde pr�c�dente, @<< annuler @>> pour annuler l'action pr�c�dente, ou @<< aide @>> pour revenir au menu d'aide !^^";
  print " Bon jeu !";
#Endif;
#Ifdef LIBRARY_ENGLISH;
  print " One last thing: at any point, you can type ~save~ to save the game, ~load~ to load a previous save, ~undo~ to undo your last move, or ~help~ to come back to this help menu!^^";
  print " Enjoy the game!";
#Endif;
  "";
];

[ author ;
#Ifdef LIBRARY_FRENCH;
  print "Je suis Hugo Labrande, cr�ateur de fictions interactives et membre de la communaut� francophone (ifiction.free.fr), sous le pseudonyme @<< Mule Hollandaise @>>. Ceci est mon ";
  print (string) number_prefix;
  print "i�me jeu.^^Pour toute remarque, rapport de bug ou critique, vous pouvez me joindre � hugo_at_hlabrande.fr.";
  "";
#Endif;
#Ifdef LIBRARY_ENGLISH;
  print "I am Hugo Labrande, writer of interactive fiction and member of the French IF community (ifiction.free.fr) as well as the English one (intfiction.org), under the alias @<< Mule Hollandaise @>>. This is my ";
  print (string) number_prefix;
  print "th game.^^For any comment, bug report or criticism, you can contact me at hugo_at_hlabrande.fr.";
  "";
#Endif;
];


#Ifdef LIBRARY_FRENCH;
Menu aidemenu "Menu"
	with topline "S�lectionnez une rubrique :";
#Endif;
#Ifdef LIBRARY_ENGLISH;
Menu aidemenu "Menu"
    with topline "Select a topic:";
#Endif;

#Ifdef LIBRARY_FRENCH;
Object -> "D�buter en fiction interactive"
#Endif;
#Ifdef LIBRARY_ENGLISH;
Object -> "Start playing interactive fiction"
#Endif;
	with description [; toBegin(); rtrue; ];

#Ifdef LIBRARY_FRENCH;
Object -> "� propos du jeu"
#Endif;
#Ifdef LIBRARY_ENGLISH;
Object -> "About the game"
#Endif;
	with description [; about(); rtrue;	];

#Ifdef LIBRARY_FRENCH;
Object -> "Auteur"
#Endif;
#Ifdef LIBRARY_ENGLISH;
Object -> "About the author"
#Endif;
	with description [; author(); rtrue; ];

Object -> "Version"
	with description [; version(); rtrue; ];
#Ifdef LIBRARY_FRENCH;
Object -> "Remerciements"
#Endif;
#Ifdef LIBRARY_ENGLISH;
Object -> "Thanks to..."
#Endif;
	with description [; thankYou(); rtrue; ];

#Ifdef LIBRARY_FRENCH;
Object -> "Licence"
#Endif;
#Ifdef LIBRARY_ENGLISH;
Object -> "License"
#Endif;
	with description [; licence(); rtrue;];
#Ifdef LIBRARY_FRENCH;
Object retour "Retour" aidemenu
#Endif;
#Ifdef LIBRARY_ENGLISH;
Object retour "Back" aidemenu
#Endif;
	with description [; return 3;];

!Global moveretour = 0;

!#ifdef Write_MoveToBottom;
!  moveretour = 1;
!#endif;

[ AideSub ;
!    if (moveretour == 1) {MoveToBottom(retour, aidemenu);}
	ShowMenu(aidemenu); rtrue;
];



#Ifdef LIBRARY_FRENCH;
Verb meta 'aide' 'sos' 'hint' 'help' 'info' 'licence' 'license' 'auteur' 'menu' 'informations' 'information' 'remerciements' 'about'
*		->Aide;

Verb meta 'a//p'
* 'propos' ->Aide
* 'propos' 'de' ->Aide
* 'propos' 'du' 'jeu' ->Aide;
#Endif;

#Ifdef LIBRARY_ENGLISH;
Verb meta 'sos' 'hint' 'help' 'info' 'license' 'author' 'menu' 'information' 'thanks' 'about'
*		->Aide;
#Endif;